**Iskolacsengő az iskolák napi csengetési rendjének lebonyolításához!**

**Telepítés**
1. Node.js telepítése https://nodejs.org/en/
2. npm install
3. npm start

Linux alá szükséges az mpg123 vagy a mpg321 csomagot telepíteni.

**Ha linux alatt a gép indulásával induljon a csengő:**
https://github.com/chovy/node-startup
https://pm2.keymetrics.io/

**Ha windows alatt a gép indulásával induljon a csengő:**
https://www.npmjs.com/package/pm2-windows-startup
https://www.npmjs.com/package/pm2-windows-service

Domokos Zoltán
Email: domokos.z@gmail.com



