var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var Timer = require('clockmaker').Timer, Timers = require('clockmaker').Timers;
var player = require('play-sound')(opts = {});
var sqlite3 = require('sqlite3').verbose();

require('./config/passport')(passport);

var bcrypt = require('bcrypt-nodejs');

var indexRouter = require('./routes/index');
var felhasznalo = require('./routes/felhasznalo');
var login = require('./routes/login');
var logout = require('./routes/logout');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(session({
    secret: 'vidyapathaisalwaysrunning',
    resave: true,
    saveUninitialized: true
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/felhasznalo', felhasznalo);
app.use('/login', login);
app.use('/logout', logout);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

// Adatbázis létrehozása
function createdb(){
var db = new sqlite3.Database('iskolacsengo.db');
db.serialize(function(){
db.run("CREATE TABLE IF NOT EXISTS csnap (id INTEGER PRIMARY KEY AUTOINCREMENT, napid INTEGER, napmegn VARCHAR (20));");
db.run("CREATE TABLE IF NOT EXISTS cslistak (id INTEGER PRIMARY KEY AUTOINCREMENT, nev VARCHAR (100), jelzo INTEGER);");
db.run("CREATE TABLE IF NOT EXISTS fokapcsolo (id INTEGER PRIMARY KEY AUTOINCREMENT, allapot INTEGER);");
db.run("CREATE TABLE IF NOT EXISTS dallam (id INTEGER PRIMARY KEY AUTOINCREMENT, utvonal VARCHAR (200), jelzo INTEGER (10), fajlnev VARCHAR (100));");
db.run("CREATE TABLE IF NOT EXISTS csrend (id INTEGER PRIMARY KEY AUTOINCREMENT, rend STRING, datum VARCHAR (8), megjegyzes VARCHAR (20), jelzo INTEGER);");
db.run("CREATE TABLE IF NOT EXISTS felhasznalok (id INTEGER PRIMARY KEY AUTOINCREMENT, status int (50) DEFAULT NULL, username char (20) NOT NULL DEFAULT '', password char (200) DEFAULT NULL, fullname char (50) DEFAULT NULL, email varchar (100) NOT NULL, roles INT);");
db.run("insert or replace into csnap (id,napid,napmegn) values(1,1,'Hétfő');");
db.run("insert or replace into csnap (id,napid,napmegn) values(2,1,'Kedd');");
db.run("insert or replace into csnap (id,napid,napmegn) values(3,1,'Szerda');");
db.run("insert or replace into csnap (id,napid,napmegn) values(4,1,'Csütörtök');");
db.run("insert or replace into csnap (id,napid,napmegn) values(5,1,'Péntek');");
db.run("insert or replace into csnap (id,napid,napmegn) values(6,0,'Szombat');");
db.run("insert or replace into csnap (id,napid,napmegn) values(7,0,'Vasárnap');");
db.run("insert or replace into fokapcsolo (id,allapot) values(1,0);");
db.run("insert or replace into dallam (id,utvonal,jelzo,fajlnev) values(1,'upload/csengo.mp3',1,'csengo.mp3');");
var passwd = bcrypt.hashSync('admin');

var query1 = "SELECT COUNT(username) AS count FROM felhasznalok where username='admin'";
    //res.charset = 'utf-8';
db.all(query1, function (err, rows) {
if(rows[0].count==0){
  var db = new sqlite3.Database('iskolacsengo.db');
  var query = "INSERT INTO felhasznalok (status,roles,username,password,fullname,email) values (1,1,'admin','"+passwd+"','domokoszoltan','domokos.z@gmail.com')";
      db.all(query, function(err){
      });
}
});   
db.close();
});
}
//

app.locals.timer = new Timer(function (timer, cb) {
    var db = new sqlite3.Database('iskolacsengo.db');
    db.each("SELECT id, allapot FROM fokapcsolo where id=1", function (err, row) {
            if (row.allapot == 1) {
                CsRend(function (err, csrend) {
                    CsNap(function (err, csnap) {
                        Dallam(function (err, dallam) {
                            var date = new Date();
                            var hour = date.getHours();
                            var min = ("0" + date.getMinutes()).substr(-2);
                            var sec = ("0" + date.getSeconds()).substr(-2);
                            var ido = hour + "." + min + "." + sec;
                            for (var csn in csnap) {                                
                                if (csnap[csn].napid == 1) {
                                    for (var csr in csrend) {
                                        if (ido == csrend[csr].datum) {
                                        for (var ddallam in dallam) {
                                            if (dallam[ddallam].jelzo == 1) {
                                               console.log("Csengetés: " + hour + ":" + min + ":" + sec);
                                               player.play(dallam[ddallam].utvonal, function (err) { });
                                            }
                                            }
                                        }
                                    }
                                }
                            }
                
                        });
                    });
                });
        }
    });
    db.close();
    cb();
}, 1000, {
    repeat: true,
    async: true
});

createdb();
app.locals.timer.start();

function Fokapcsolo(cb) {
    var db = new sqlite3.Database('iskolacsengo.db');
    db.prepare("SELECT id,allapot FROM fokapcsolo where id=1", function (err, fokapcsolo) {
        cb(null, fokapcsolo)
    });
    db.close();
}

function CsRend(cb) {
    var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM csrend,cslistak WHERE csrend.rend = cslistak.id and cslistak.jelzo = 1 and csrend.jelzo = 1", function (err, csrend) {
        cb(null, csrend)
    });
    db.close();
}

function CsNap(cb) {
    var db = new sqlite3.Database('iskolacsengo.db');
    var date = new Date();
    var napid = date.getDay();
    var q = "SELECT * FROM csnap where id=" + napid;
    db.all(q, function (err, csnap) {
        cb(null, csnap)
    });
    db.close();
}
function Dallam(cb) {
    var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM dallam", function (err, dallam) {
        cb(null, dallam)
    });
    db.close();
}



module.exports = app;
