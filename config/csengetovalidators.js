const { check, validationResult } = require('express-validator');

class csengetovalidator{    

  fokapcsol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });   
    }

  csnappost(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('napid').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        }); 
        });    
    }

  dallamkapcsol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('jelzo').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        }); 
        });    
    }

  dallampost(req, res,next) {   
        check('file.originalname').isLength({ max: 30 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });  
    }

  dallamtorol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  csbeir(req, res,next) {   
        check('nev').isLength({ max: 30 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  cslista(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  jcsbeir(req, res,next) {   
        check('azon').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('nev').isLength({ max: 30 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        }); 
        });    
    }

  cstorol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  cslistakt(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('jelzo').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        }); 
        });    
    }

  csrend(req, res,next) {   
        check('rend').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  cslbeir(req, res,next) {   
        check('ido').isLength({ min: 6 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('megjegyzes').isLength({ max: 30 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });
        });
        });    
    }

  jcslform(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  jcslbeir(req, res,next) {   
        check('ido').isLength({ min: 6 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('megjegyzes').isLength({ max: 30 }).withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('azon').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });
        });
        });    
    }

  csltorol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
    }

  csljupdate(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('jelzo').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }
        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        }); 
        });    
    }

}

module.exports = new csengetovalidator 