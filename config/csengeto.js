var sqlite3 = require('sqlite3').verbose();
var multer = require('multer');
var fs = require('fs');
const { check, validationResult } = require('express-validator');

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './upload/');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
    console.log(file);
  }
});

var upload = multer({ storage : storage}).single('utvonal');

class csengeto{	

  index(req, res,next) {	
    res.render('index', { title: 'Csengetési rend',felh: req.user.username });	
  }

  fokapcsololist(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    db.all("SELECT * FROM fokapcsolo", function (err, fokapcsolo) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(fokapcsolo);
        }
    });
    db.close();
  }

  fokapcsol(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    db.all("update fokapcsolo set allapot=" + req.query.id +" where id=1", function (err, fokapcsolo) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();	
  }
	
  csnapget(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    db.all("SELECT * FROM csnap", function (err, csnap) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(csnap);
        }
    });
    db.close();
  }

  csnappost(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    db.all("update csnap set napid="+ req.body.napid +" where id=" + req.body.id, function (err, csnap) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();		
  }
	
  dallamget(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM dallam", function (err, dallam) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(dallam);
        }
    });
    db.close();
  }	

  dallamkapcsol(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
    db.all("update dallam set jelzo="+ req.body.jelzo +" where id=" + req.body.id, function (err, dallam) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();	
  }
	
  dallampost(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
          upload(req,res,function(err) {
            if (err){
				res.json({muv: 1});
			}
			else{
				
				var sql = "INSERT INTO dallam (utvonal,fajlnev,jelzo) VALUES('upload/" + req.file.originalname + "','"+ req.file.originalname+"',0)";
                db.run(sql, function (err) {
					if (err) {
						res.json({muv: 1});
					}
					else {
						res.json({muv: 0});
					}
               });
               db.close();			   
			}      
    });
  }

  dallamtorol(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    db.each('SELECT * FROM dallam where id=' + req.query.id, function (err, row) {
    fs.unlink(row.utvonal, function () {
        if (err){
			res.json({muv: 1});
		}
		else{
			var query = 'DELETE FROM dallam where id=' + req.query.id;
            db.run(query, function (err, row) {
				if (err) {
					res.json({muv: 1});
				}
				else {
					res.json({muv: 0});
				}
           });
           db.close();
		}
        });
    });	
  }
	
  cslistak(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM cslistak", function (err, cslistak) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(cslistak);
        }

    });
    db.close();
  }	

  csbeir(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
    var sql = "INSERT INTO cslistak (nev,jelzo) VALUES('"+ req.body.nev +"',0)";
    db.run(sql, function (err) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }	

  cslista(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM cslistak where id=" + req.query.id, function (err, cslista) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(cslista);
        }
    });
    db.close();
  }	

  jcsbeir(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    var sql = "UPDATE cslistak SET nev='"+ req.body.nev+"' where id=" + req.body.azon;
    db.all(sql, function (err, rows, fields) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close()
  }	

  cstorol(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
	var query1 = 'DELETE FROM csrend where rend=' + req.query.id;
    var query = 'DELETE FROM cslistak where id=' + req.query.id;
    db.run(query1, function (err, row) {
        if (err) {
            res.json({muv: 1});
        }
        else {
         db.run(query, function (err, row) {
         if (err) {
            res.json({muv: 1});
         }
         else {
            res.json({muv: 0});
         }
		});
		}
    });
    db.close();
  }

  cslistakt(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    var sql = "UPDATE cslistak SET jelzo = "+ req.body.jelzo+" where id=" + req.body.id;
    db.all(sql, function (err, row) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

	csrend(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM csrend where rend="+ req.query.rend, function (err, csrend) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(csrend);
        }
    });
    db.close();
  }	

  cslbeir(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    var sql = "INSERT INTO csrend (rend,datum,megjegyzes,jelzo) VALUES('" + req.body.id + "','" + req.body.ido + "','" + req.body.megjegyzes + "',1)";
    db.run(sql, function (err) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

  jcslform(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT * FROM csrend where id=" + req.query.id, function (err, csrend) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(csrend);
        }
    });
    db.close();
  }

  jcslbeir(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    var sql = "UPDATE csrend SET datum = '" + req.body.ido + "',megjegyzes = '" + req.body.megjegyzes + "'  where id=" + req.body.azon;
    db.run(sql, function (err, row) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

  csltorol(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db');
    var query = 'DELETE FROM csrend where id=' + req.query.id;
    db.run(query, function (err, row) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

  csljupdate(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');   
    var sql = "UPDATE csrend SET jelzo = "+ req.body.jelzo+" where id=" + req.body.id;
    db.run(sql, function (err, row) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

}

module.exports = new csengeto 