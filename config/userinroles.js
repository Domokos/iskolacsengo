function isLoggedIn(req, res, next) {
    	//console.log(req.baseUrl + req.path);
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    
    // if they aren't redirect them to the home page
    res.redirect('/login');
}

module.exports = isLoggedIn;