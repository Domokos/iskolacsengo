var sqlite3 = require('sqlite3').verbose();
var bcrypt = require('bcrypt-nodejs');
const { check, validationResult } = require('express-validator');

class felhasznalo{	
  index(req, res,next) {	
    res.render('felhasznalo/felhasznalo', { title: 'Felhasználókezelés',felh: req.user.username });	
  }

  list(req, res,next) {	
    var db = new sqlite3.Database('iskolacsengo.db');
    db.all("SELECT  id,fullname,username,status,roles,email FROM felhasznalok", function (err, felhasznalok) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(felhasznalok);
        }
    });
    db.close();
  }

  add(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    var passwd = bcrypt.hashSync(req.body.password);
    var query = "INSERT INTO felhasznalok (fullname,username,password,status,email,roles) values ('"+req.body.fullname+"','"+req.body.username+"','"+passwd+"','"+req.body.status+"','"+req.body.email+"','"+req.body.roles+"')";
        db.run(query, function (err) {

        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });	
    db.close();
  }

  pass(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    var passwd = bcrypt.hashSync(req.body.password);
    var query = "UPDATE felhasznalok SET password='"+ passwd+"' where id=" + req.body.id;
    db.all(query, function (err, rows, fields) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();
  }

  edit(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    var passwd = bcrypt.hashSync(req.body.password);
    var query = "UPDATE felhasznalok SET fullname='"+ req.body.fullname+"',username='"+ req.body.username+ "',status='"+ req.body.status+ "',email='"+ req.body.email+ "',roles='"+ req.body.roles+ "' where id=" + req.body.id;
    db.all(query, function (err, rows, fields) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });	
    db.close();
  }

  torol(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    var query = 'DELETE FROM felhasznalok where id=' + req.query.id;
    db.all(query, function (err, rows, fields) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json({muv: 0});
        }
    });
    db.close();   
  }	

  edite(req, res,next) {	
	var db = new sqlite3.Database('iskolacsengo.db'); 
    var query = 'SELECT id,fullname,username,status,roles,email FROM felhasznalok where id=' + req.query.id;
    db.all(query, function (err, rows, fields) {
        if (err) {
            res.json({muv: 1});
        }
        else {
            res.json(rows);
        }
    });
    db.close();
  }

}

module.exports = new felhasznalo 