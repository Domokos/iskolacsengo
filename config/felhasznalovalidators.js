const { check, validationResult } = require('express-validator');

class felhasznalovalidator{    

  add(req, res,next) {   
        check('username').isLength({ min: 6 }).withMessage('must be a valid email')(req, res, err => {
            if (err) {
                return next(err);
            }

        check('fullname').isLength({ min: 6 }).withMessage('passwd 4 chars long!')(req, res, err => {
            if (err) {
                return next(err);
            }

        check('email').isEmail().withMessage('must be a valid email')(req, res, err => {
            if (err) {
                return next(err);
            }

        check('password').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i").withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
            }

        check('status').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
            }

        check('roles').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
            } 

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });
        });     
        });
        });
        });
        });
  }

  pass(req, res,next) {   
        check('password').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i").withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
            }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });   
  }

  edit(req, res,next) {   
        check('username').isLength({ min: 6 }).withMessage('must be a valid email')(req, res, err => {
            if (err) {
                return next(err);
        }

        check('fullname').isLength({ min: 6 }).withMessage('passwd 4 chars long!')(req, res, err => {
            if (err) {
                return next(err);
            }
        check('email').isEmail().withMessage('must be a valid email')(req, res, err => {
            if (err) {
                return next(err);
        }

        check('status').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        check('roles').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
            }  

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });
        });
        });
        });
        });     
  }

  torol(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
  }

  edite(req, res,next) {   
        check('id').isNumeric().withMessage(1)(req, res, err => {
            if (err) {
                return next(err);
        }

        try {
            validationResult(req).throw();
            next();
        } catch (err) {
            res.status(422).json({ errors: err.mapped() });
        }
        });    
  }

}

module.exports = new felhasznalovalidator 