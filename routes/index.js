var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var isLoggedIn = require('../config/userinroles.js');
var csengeto = require('../config/csengeto.js');
var csengetovalidator = require('../config/csengetovalidators.js');

router.get('/', isLoggedIn, csengeto.index);

router.get('/fokapcsololist', isLoggedIn, csengeto.fokapcsololist);

router.get('/fokapcsol', isLoggedIn, csengetovalidator.fokapcsol, csengeto.fokapcsol);

router.get('/csnap', isLoggedIn, csengeto.csnapget);

router.post('/csnap', isLoggedIn, csengetovalidator.csnappost, csengeto.csnappost);

router.get('/dallam', isLoggedIn, csengeto.dallamget);

router.post('/dallamkapcsol', isLoggedIn, csengetovalidator.dallamkapcsol, csengeto.dallamkapcsol);

router.post('/dallam', isLoggedIn,csengetovalidator.dallampost, csengeto.dallampost);

router.get('/dallamtorol', isLoggedIn, csengetovalidator.dallamtorol, csengeto.dallamtorol);

router.get('/cslistak', isLoggedIn, csengeto.cslistak);

router.post('/csbeir', isLoggedIn, csengetovalidator.csbeir, csengeto.csbeir);

router.get('/cslista', isLoggedIn,  csengetovalidator.cslista, csengeto.cslista);

router.post('/jcsbeir', isLoggedIn, csengetovalidator.jcsbeir, csengeto.jcsbeir);

router.get('/cstorol', isLoggedIn, csengetovalidator.cstorol, csengeto.cstorol);

router.post('/cslistakt', isLoggedIn, csengetovalidator.cslistakt, csengeto.cslistakt);

router.get('/csrend', isLoggedIn, csengetovalidator.csrend, csengeto.csrend);

router.post('/cslbeir', isLoggedIn, csengetovalidator.cslbeir, csengeto.cslbeir);

router.get('/jcslform', isLoggedIn, csengetovalidator.jcslform, csengeto.jcslform);

router.post('/jcslbeir', isLoggedIn, csengetovalidator.jcslbeir, csengeto.jcslbeir);

router.get('/csltorol', isLoggedIn, csengetovalidator.csltorol, csengeto.csltorol);

router.post('/csljupdate', isLoggedIn, csengetovalidator.csljupdate, csengeto.csljupdate);

module.exports = router;
