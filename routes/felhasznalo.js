var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
var bcrypt = require('bcrypt-nodejs');
var isLoggedIn = require('../config/userinroles.js');
var felhasznalo = require('../config/felhasznalo.js');
var felhasznalovalidator = require('../config/felhasznalovalidators.js');

router.get('/', isLoggedIn, felhasznalo.index);

router.get('/list', isLoggedIn, felhasznalo.list);

router.post('/add', isLoggedIn, felhasznalovalidator.add, felhasznalo.add);

router.post('/pass', isLoggedIn, felhasznalovalidator.pass, felhasznalo.pass);

router.post('/edit', isLoggedIn, felhasznalovalidator.edit, felhasznalo.edit);

router.get('/torol', isLoggedIn, felhasznalovalidator.torol, felhasznalo.torol);


router.get('/edite', isLoggedIn,felhasznalovalidator.edite, felhasznalo.edite);

module.exports = router;
