﻿var express = require('express');
var router = express.Router();
var passport = require('passport');
/* GET home page. */
router.get('/', function (req, res) {
    res.render('login', {
        title: 'Bejelentkezés',
        message: req.flash('loginMessage')
    });
});

router.post('/', passport.authenticate('local-login', {
    successRedirect : '/', 
    failureRedirect : '/login', 
    failureFlash : true 
}),function (req, res) {
    res.render('login', {
        title: 'Bejelentkezés',
        message: req.flash('loginMessage')
    });
});

function isLoggedIn(req, res, next) {
    
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    
    // if they aren't redirect them to the home page
    res.redirect('/login');
}

module.exports = router;