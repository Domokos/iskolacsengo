﻿$(document).ready(function () {

//$("#ido").inputmask({"mask":"99.99.99"});
$("#ido").inputmask('99.99.99');
$("#jcsrido").inputmask('99.99.99');

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
	
    $(function(){
	  $.ajax({
		dataType: "json",
		url: "/fokapcsololist",
		success: function( data ) {
          if(data[0].allapot==1){
			  $("#fokapcsolo").bootstrapToggle('on');
		  }
          if(data[0].allapot==0){
			  $("#fokapcsolo").bootstrapToggle('off');;
		  }
		}
	  });
	  
	  $.ajax({
		dataType: "json",
		url: "/csnap",
		success: function( data ) {
			var html = '<ul style="list-style-type: none; margin: 0; padding: 0;">';
            jQuery.each( data, function( i, val ) {
				if(val.napid==0){
			        html += '<li style="padding-top: 10px;"><input id="csnapok" name="csnapok" value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.napmegn+'</li>' ;
				}
				else{
					html += '<li style="padding-top: 10px;"><input id="csnapok" name="csnapok" checked value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.napmegn+'</li>' ;
				}
			   
			});
			html += '</ul>';
			$('#csnaplista1').html( html );
			$("input[id^='csnapok']").bootstrapToggle();
		}
	  });

      dallamlista();
	  csengetesilista();
   });
   
   function csengetesilista(){
	  $.ajax({
		dataType: "json",
		url: "/cslistak",
		success: function( data ) {
			var html ='';
            jQuery.each( data, function( i, val ) {
				if(val.jelzo==0){
			        html += '<li style="padding-top: 30px;"><input id="cslistak" name="cslistak" value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.nev+'\
			          <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#cslistajavit"><span class="fas fa-user-edit" aria-hidden="true"/></a>\
			          <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#cslistatorol"><span class="fas fa-user-times" aria-hidden="true" /></a></li>\
					' ;
					html += csengetesirend(val.id);
				}
				else if(val.jelzo==1){
					html += '<li style="padding-top: 30px;"><input id="cslistak" name="cslistak" checked value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.nev+'\
			        <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#cslistajavit"><span class="fas fa-user-edit" aria-hidden="true"/></a>\
			        <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#cslistatorol"><span class="fas fa-user-times" aria-hidden="true" /></a></li>\
					' ;
					html += csengetesirend(val.id);
				}
			   
			});
			$('#csengetesilista1').html( html );
			$("input[id^='cslistak']").bootstrapToggle();
			$("input[id^='csrend']").bootstrapToggle();
		}
	  });
   }
   
   function csengetesirend(rend){	   
	var html = '<button style="margin-top: 30px;" id="csrendhozzaadas" type="button" class="btn btn-primary" data-whatever="'+rend+'" data-toggle="modal" data-target="#csrendhozzaad">Hozzáadás</button>';	
    $.ajax({
        type: "GET",
		async: false,
        url: "/csrend?rend=" + rend,
        dataType: 'json',
        success: function(data) {
		 jQuery.each( data, function( i, val ) {
			 if(val.megjegyzes==null){
				var  megjegyzes='';
			 }
			 else{
				var  megjegyzes=val.megjegyzes;
			 }
			 if(val.jelzo==0){
			 html += '<div class="row" style="padding-top: 10px; padding-left: 20px;">\
			 <div class="col-sm-2"><input id="csrend" name="csrend" value="'+val.id+'" type="checkbox" data-toggle="toggle" data-size="xs"> '+val.datum+'</div>\
			 <div class="col-sm-6">'+ megjegyzes +'</div>\
			 <div class="col-sm-2">\
			 <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#csrendjavit"><span class="fas fa-user-edit" aria-hidden="true"/></a>\
			 <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#tcsrtorlol"><span class="fas fa-user-times" aria-hidden="true" /></a>\
			 </div>\
			 </div>';	
             }
             else if(val.jelzo==1){
			 html += '<div class="row" style="padding-top: 10px; padding-left: 20px;">\
			 <div class="col-sm-2"><input id="csrend" name="csrend" checked value="'+val.id+'" type="checkbox" data-toggle="toggle" data-size="xs"> '+val.datum+'</div>\
			 <div class="col-sm-6">'+ megjegyzes +'</div>\
			 <div class="col-sm-2">\
			 <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#csrendjavit"><span class="fas fa-user-edit" aria-hidden="true"/></a>\
			 <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#tcsrtorlol"><span class="fas fa-user-times" aria-hidden="true" /></a>\
			 </div>\
			 </div>';
			 }			 
		 });         
        }
    });
	return html;	   	 	 	         
   }
   
   function dallamlista(){
	  $.ajax({
		dataType: "json",
		url: "/dallam",
		success: function( data ) {
			var html = '<ul style="list-style-type: none; margin: 0; padding: 0;">';
            jQuery.each( data, function( i, val ) {
				if(val.jelzo==0){
			        html += '<li style="padding-top: 10px;"><input id="dallam" name="dallam" value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.fajlnev+' <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#dallamtorol"><i class="fa fa-trash fa-2" aria-hidden="true"></i></a></li>' ;
				}
				else{
					html += '<li style="padding-top: 10px;"><input id="dallam" name="dallam" checked value="'+val.id+'" type="checkbox" data-toggle="toggle"> '+val.fajlnev+' <a href="#" data-toggle="modal" data-whatever="'+val.id+'" data-target="#dallamtorol"><i class="fa fa-trash fa-2" aria-hidden="true"></i></a></li>' ;
				}
			   
			});
			html += '</ul>';
			$('#dallamlista1').html( html );
			$("input[id^='dallam']").bootstrapToggle();
		}
	  });
   }
   
   $('#dallamtorol').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        $('#tazon').val(recipient);
   });
 
   $("#torlesdallam").click(function () {

        var formData = {
            'id': $('#tazon').val()
        };

        $.ajax({
            url: "/dallamtorol",
            type: 'get',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#dallamtorol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    var id = $('#tazon').val();
                    dallamlista();
                }
                else {
                    $('#dallamtorol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    })
	
   $("#dallamkuldes").click(function () {

var formData = new FormData();
        var data = new FormData();
        var files = $('#utvonal')[0].files[0];
        data.append('utvonal',files);



        $.ajax({
            url: "/dallam",
            type: 'post',
			method: 'post',
			enctype: 'multipart/form-data',
            data: data,
            processData: false,
            contentType: false, 
            success: function (result) {
                if (result.muv == 0) {
                    $('#dallamfelvitel').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A zene felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    dallamlista();
                }
                else {
                    $('#dallamfelvitel').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A zene felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    })
	
$(document.body).on('change',"#dallam",function (e) {
        var allapot = $(this).val();
        $("#dallam").each(function () {
		if($(this).prop('checked')){
			var id =1;
		}else{
			var id =0;
		}
		$.post( "/dallamkapcsol",{ id: allapot, jelzo: id } , function( data ) {		
		});
		});
});
 
$(document.body).on('change',"#csnapok",function (e) {
	    var allapot = $(this).val();
		if($(this).prop('checked')){
			var id =1;
		}else{
			var id =0;
		}
		$.post( "/csnap",{ napid: id, id: allapot } , function( data ) {		
			
		});
});

$("#fokapcsolo").change(function() {
        var type = $(this).prop("checked");
        if (type == true) { 
            var id = 1;
        }
        else {
            var id = 0;
        }
        $.get("/fokapcsol?id=" + id , function (data) {
        });
});

   $("#cslistahozzaadas").click(function () {

        var formData = {
            'nev': $('#nev').val()
        };

        var form = $( "#cslistaaddForm" );
        form.validate();  

		if(form.valid()){

        $.ajax({
            url: "/csbeir",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#cslistahozzaad').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    csengetesilista();
                    form.trigger("reset");
                }
                else {
                    $('#cslistahozzaad').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
		}
    });

    $('#cslistajavit').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var kereso = '/cslista?id=' + recipient;
        $.getJSON(kereso, function (data1) {
            if (data1.length == 0 && data1.value == null) {

            }
            else {
                $('#azon').val(data1[0].id);
                $('#nevj').val(data1[0].nev);
            }
        });
    });

   $("#cslistajavitas").click(function () {

        var formData = {
            'azon': $('#azon').val(),
			'nev': $('#nevj').val()
        };

        var form = $( "#cslistajavitForm" );
        form.validate();  

		if(form.valid()){

        $.ajax({
            url: "/jcsbeir",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#cslistajavit').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    csengetesilista();
                    form.trigger("reset");
                }
                else {
                    $('#cslistajavit').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
		}
    })
	
    $('#cslistatorol').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        $('#tazon').val(recipient);
    });
	
    $("#torles").click(function () {

        var formData = {
            'id': $('#tazon').val()
        };

        $.ajax({
            url: "/cstorol",
            type: 'get',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#cslistatorol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    var id = $('#tazon').val();
                    csengetesilista();
                }
                else {
                    $('#cslistatorol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    })

   $("#csrendhozzaadas").click(function () {
        var formData = {
            'id': $('#cslazon').val(),
			'ido': $('#ido').val(),
			'megjegyzes': $('#megjegyzes').val(),
        };

        var form = $( "#csrendaddform" );
        form.validate();  

		if(form.valid()){

        $.ajax({
            url: "/cslbeir",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#csrendhozzaad').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    csengetesilista();
                    form.trigger("reset");
                }
                else {
                    $('#csrendhozzaad').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
		}
    });
	
    $('#csrendhozzaad').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var recipient = button.data('whatever');
        $('#cslazon').val(recipient);
    });
	
    $('#csrendjavit').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var kereso = '/jcslform?id=' + recipient;
        $.getJSON(kereso, function (data1) {
            if (data1.length == 0 && data1.value == null) {

            }
            else {
                $('#jcsrazon').val(data1[0].id);
                $('#jcsrido').val(data1[0].datum);
                $('#jcsrmegjegyzes').val(data1[0].megjegyzes);
            }
        });
    });

   $("#csrendjavitas").click(function () {
        var formData = {
            'azon': $('#jcsrazon').val(),
			'ido': $('#jcsrido').val(),
			'megjegyzes': $('#jcsrmegjegyzes').val(),
        };

        var form = $( "#csrendaddform" );
        form.validate();  

		if(form.valid()){

        $.ajax({
            url: "/jcslbeir",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#csrendjavit').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    csengetesilista();
                    form.trigger("reset");
                }
                else {
                    $('#csrendjavit').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
		}
    });
	
	$("#tcsrtorles").click(function () {

        var formData = {
            'id': $('#tcsrazon').val()
        };

        $.ajax({
            url: "/csltorol",
            type: 'get',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#tcsrtorlol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    var id = $('#tazon').val();
                    csengetesilista();
                }
                else {
                    $('#tcsrtorlol').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    })
	
    $('#tcsrtorlol').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var recipient = button.data('whatever');
        $('#tcsrazon').val(recipient);
    });
	
	$(document.body).on('change',"#cslistak",function (e) {
	    var allapot = $(this).val();
		if($(this).prop('checked')){
			var id =1;
		}else{
			var id =0;
		}
		$.post( "/cslistakt",{ jelzo: id, id: allapot } , function( data ) {		
			
		});
    });
   
   	$(document.body).on('change',"#csrend",function (e) {
	    var allapot = $(this).val();
		if($(this).prop('checked')){
			var id =1;
		}else{
			var id =0;
		}
		$.post( "/csljupdate",{ jelzo: id, id: allapot } , function( data ) {		
			
		});
    });

});
