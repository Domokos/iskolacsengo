﻿$(document).ready(function () {

jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});

function list(){
	  $.ajax({
		dataType: "json",
		url: "/felhasznalo/list",
		success: function( data ) {

          $("#movieList").empty();

          $("#movieTemplate").tmpl(data).appendTo("#movieList");
		}
	  });
	
}
list();

   $("#userfullWrite").click(function () {

        var formData = {
            'email': $('#aemail').val(),
            'username': $('#ausername').val(),
            'fullname': $('#afullname').val(),
            'password': $('#apassword').val(),
            'roles': $('#aroles').val(),
            'status': $('#astatus').val()
        };

        var form = $( "#useraddForm" );
        form.validate();  

        if(form.valid()){      

        $.ajax({
            url: "felhasznalo/add",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#UserAddModal').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    list();
                }
                else {
                    $('#UserAddModal').modal('hide');
                    $("#messt").text("Felvitel");
                    $("#mess").text("A felvitel sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    }
    });

   $("#userWrite").click(function () {

        var formData = {
            'azon': $('#azon').val(),
            'email': $('#email').val(),
            'username': $('#username').val(),
            'fullname': $('#fullname').val(),
            'roles': $('#roles').val(),
            'status': $('#status').val()
        };

        var form = $( "#userForm" );
        form.validate(); 

        if(form.valid()){       

        $.ajax({
            url: "felhasznalo/edit",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#UserModal').modal('hide');
                    $("#messt").text("Adat frissítés");
                    $("#mess").text("Az adat frissítés sikerült!");
                    $('#bs-example-modal-t').modal('show');
					var azon = $('#azon').val();
                    list();
                }
                else {
                    $('#UserModal').modal('hide');
                    $("#messt").text("Adat frissítés");
                    $("#mess").text("A adat frissítés sikertelen volt!");
                    //$('#bs-example-modal-t').modal('show');
                }
            }
        });
    }
    })

   $("#passwordWrite").click(function () {

        var formData = {
            'azon': $('#pazon ').val(),
            'password': $('#ppassword').val()

        };

        var form = $( "#passwordForm" );
        form.validate(); 

        if(form.valid()){

        $.ajax({
            url: "felhasznalo/pass",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#passwordModal').modal('hide');
                    $("#messt").text("Jelszó frissítés");
                    $("#mess").text("A jelszó frissítés sikerült!");
                    $('#bs-example-modal-t').modal('show');
                }
                else {
                    $('#passwordModal').modal('hide');
                    $("#messt").text("Jelszó frissítés");
                    $("#mess").text("A jelszó frissítés sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
        }
    })



    $('#UserModal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var kereso = '/felhasznalo/edite?id=' + recipient;
        $.getJSON(kereso, function (data1) {
            if (data1.length == 0 && data1.value == null) {

            }
            else {
                $('#azon').val(data1[0].id);
                $('#email').val(data1[0].email);
                $('#fullname').val(data1[0].fullname);
                $('#username').val(data1[0].username);
                $('#roles').val(data1[0].roles).change();
                $('#status').val(data1[0].status).change();
            }
        });
    });



    $('#passwordModal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        $('#pazon').val(recipient);
    });

        $('#TorolModal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        $('#tazon').val(recipient);
    });


    $("#torles").click(function () {

        var formData = {
            'azon': $('#tazon').val()
        };

        $.ajax({
            url: "felhasznalo/torol",
            type: 'post',
            data: formData,
            success: function (result) {
                if (result.muv == 0) {
                    $('#TorolModal').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikerült!");
                    $('#bs-example-modal-t').modal('show');
                    var id = $('#tazon').val();
                    //$('[data-whatever='+id+']').parents('tr').remove();
                    list();
                }
                else {
                    $('#TorolModal').modal('hide');
                    $("#messt").text("Törlés");
                    $("#mess").text("A törlés sikertelen volt!");
                    $('#bs-example-modal-t').modal('show');
                }
            }
        });
    })


});
